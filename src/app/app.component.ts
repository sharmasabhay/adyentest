import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
//@ts-ignore
import AdyenCheckout from '@adyen/adyen-web';

import { environment } from '../environments/environment';

import {payment} from './607e73b59ae56e891c8b457c';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'sampleApp';
  @ViewChild('hook', { static: true }) hook: ElementRef;
  type: string = '';
  clientKey: string = environment.clientKey as string;

  constructor() {
    this.hook = new ElementRef('');
  }

  ngOnInit(): void {
    const configuration = {
      paymentMethodsResponse: payment,
      clientKey: this.clientKey,
      locale: 'en_US',
      environment: 'test',
      showPayButton: true,
      paymentMethodsConfiguration: {
        ideal: {
          showImage: true,
        },
        card: {
          hasHolderName: true,
          holderNameRequired: true,
          name: 'Credit or debit card',
          amount: {
            value: 1000,
            currency: 'EUR',
          },
        },
      },
      onSubmit: (state: any, component: any) => {
        if (state.isValid) {
          // this.initiatePayment(state, component);
        }
      },
      onAdditionalDetails: (state: any, component: any) => {
        // this.submitAdditionalDetails(state, component);
      },
    };
    
    //@ts-ignore
    const checkout = new AdyenCheckout(configuration);

    checkout.create('dropin').mount(this.hook.nativeElement);    
  }
}
