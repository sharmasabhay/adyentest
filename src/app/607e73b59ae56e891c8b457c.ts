export const payment = {
  "paymentMethods": [
    {
      "brands": [
        "visa",
        "mc",
        "amex",
        "jcb",
        "diners",
        "discover"
      ],
      "details": [
        {
          "key": "encryptedCardNumber",
          "type": "cardToken"
        },
        {
          "key": "encryptedSecurityCode",
          "type": "cardToken"
        },
        {
          "key": "encryptedExpiryMonth",
          "type": "cardToken"
        },
        {
          "key": "encryptedExpiryYear",
          "type": "cardToken"
        },
        {
          "key": "holderName",
          "optional": true,
          "type": "text"
        }
      ],
      "name": "Credit Card",
      "type": "scheme"
    },
    {
      "configuration": {
        "merchantId": "1000",
        "gatewayMerchantId": "MISCGroupPtdLtdECOM"
      },
      "details": [
        {
          "key": "paywithgoogle.token",
          "type": "payWithGoogleToken"
        }
      ],
      "name": "Google Pay",
      "type": "paywithgoogle"
    },
    {
      "name": "UnionPay",
      "type": "unionpay"
    },
    {
      "configuration": {
        "merchantId": "000000000200560",
        "merchantName": "MISCGroupPtdLtdECOM"
      },
      "details": [
        {
          "key": "applepay.token",
          "type": "applePayToken"
        }
      ],
      "name": "Apple Pay",
      "type": "applepay"
    }
  ],
  "storedPaymentMethods": [
    {
      "brand": "amex",
      "expiryMonth": "03",
      "expiryYear": "2030",
      "holderName": "A Sharma",
      "id": "8416188963897967",
      "lastFour": "8431",
      "name": "American Express",
      "supportedShopperInteractions": [
        "Ecommerce",
        "ContAuth"
      ],
      "type": "scheme"
    },
    {
      "brand": "visa",
      "expiryMonth": "03",
      "expiryYear": "2030",
      "holderName": "A Sharma",
      "id": "8316186385382908",
      "lastFour": "1111",
      "name": "VISA",
      "supportedShopperInteractions": [
        "Ecommerce",
        "ContAuth"
      ],
      "type": "scheme"
    }
  ]
}
